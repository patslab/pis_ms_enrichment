### SMR analyses
screen -S SMR
#conda create -n smr R=4.1
conda activate smr
#conda install -c conda-forge r-stringr
#conda install -c bioconda bioconductor-organism.dplyr
#conda install -c bioconda bioconductor-org.hs.eg.db
#conda install -c bioconda bioconductor-txdb.hsapiens.ucsc.hg38.knowngene
#conda install -c bioconda bioconductor-txdb.hsapiens.ucsc.hg19.knowngene
#conda install -c bioconda r-optparse

### --- Test with provided studies --- ###
# screen -S SMR_test

## -- Westra eQTL -- ##
/work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR  --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/westra_eqtl_hg19 --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_Westra  --thread-num 10 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_Westra.log
# 483499 SNPs are included after allele checking.
sort -gk 19 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_Westra.smr | awk '$19<0.000008519 && $20>=0.01' |  grep -v "nan" | wc -l
# only 27(!)

## -- ENDOF Westra eQTL -- ##

## -- Brain eMeta -- ##
/work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR  --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/Brain-eMeta/Brain-eMeta --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_Brain-eMeta  --thread-num 30 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_Brain-eMeta.log
# 5333363 SNPs are included after allele checking.
sort -gk 19 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_Brain-eMeta.smr | awk '$19<6.98e-06 && $20>=0.01' |  grep -v "nan" | wc -l
# 18
## -- ENDOF Brain eMeta -- ##

## -- GTEx_v8 -- ##
cat ls -lhtr /work/Tools/smr/data/GTEx_V8_cis_eqtl_summary_lite/eQTL_besd_lite/*besd | awk '{print $NF}' | sed -e 's/^.*besd_lite\///' -e 's/.lite.besd//' | tr '\n' ' '
parallel -j4 --bar '/work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR   --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/GTEx_V8_cis_eqtl_summary_lite/eQTL_besd_lite/{1}.lite --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_{1}.lite  --thread-num 10 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_{1}.lite.log' ::: `ls -lhtr /work/Tools/smr/data/GTEx_V8_cis_eqtl_summary_lite/eQTL_besd_lite/*besd | awk '{print $NF}' | sed -e 's/^.*besd_lite\///' -e 's/.lite.besd//' | tr '\n' ' '`

# get N of significant genes
echo "Tissue N_genes N_significant" > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_GTEx_lite.summary
ls -lhtr /work/Tools/smr/data/GTEx_V8_cis_eqtl_summary_lite/eQTL_besd_lite/*besd | awk '{print $NF}' | sed -e 's/^.*besd_lite\///' -e 's/.lite.besd//' | while read tissue
do
 echo -n $tissue
 # get n of tested genes
 n=`awk 'NR>1' /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_"$tissue".lite.smr | grep -v "nan" | wc -l `
 echo -n " "$n" "
 sort -gk 19 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_"$tissue".lite.smr | awk -v n=$n '$19<(0.05/n) && $20>=0.01' |  grep -v "nan"  > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_"$tissue".lite.smr.top
 wc -l /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_"$tissue".lite.smr.top | awk '{print $1}'
done >> /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_GTEx_lite.summary



### -- DICE -- ###
## all results are here: /archive/data/dbGAP/DICE/eQTL_res/
## they used vcf format to store the results
## the results do not have TSS position. However, we do need to add the TSS position. We will format this as QTLtools output
## This file has no header. the 14 columns are The columns are: 1. The phenotype ID 2. The chromosome ID of the phenotype 3. The start position of the phenotype 4. The end position of the phenotype 5. The strand orientation of the phenotype 6. The total number of variants tested in cis 7. The distance between the phenotype and the tested variant (accounting for strand orientation) 8. The ID of the tested variant 9. The chromosome ID of the variant 10. The start position of the variant 11. The end position of the variant 12. The nominal P-value of association between the variant and the phenotype 13. The corresponding regression slope 14. A binary flag equal to 1 is the variant is the top variant in cis.

## move to dir where we store SMR formatted data
cd /work/Tools/smr/data
mkdir DICE
cd DICE

## split per chromosome, so we can parallelize processing
# Th17
zcat /archive/data/dbGAP/DICE/eQTL_res/TH17.vcf.gz | awk 'NR>8 {print $1, $2, $3, $4, $5, $8 > $1".Th17.tmp"}'
# naive B cells
zcat /archive/data/dbGAP/DICE/eQTL_res/B_CELL_NAIVE.vcf.gz | awk 'NR>8 {print $1, $2, $3, $4, $5, $8 > $1".nB.tmp"}'

### other cells 
for i in TREG_NAIVE  TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE 
do 
 echo $i
 zcat /archive/data/dbGAP/DICE/eQTL_res/$i.vcf.gz | awk -v i=$i 'NR>8 {print $1, $2, $3, $4, $5, $8 > $1"."i".tmp"}'  
done

## Prepare file in QTLtools format
# main file. This queries biomart and cannot be paralellized
# for j in Th17 nB TREG_NAIVE
for j in  TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 echo $j
 for i in {1..22}
 do
  echo $i
   Rscript /work/Tools/smr/data/DICE/SMR_format.R -i /work/Tools/smr/data/DICE/chr$i.$j.tmp -o /work/Tools/smr/data/DICE/chr$i.$j.qtltools -g /work/Tools/smr/data/DICE/chr$i.$j.all.txt
 done
done

# join all chromosomes in one file
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in  NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 awk 'NR>1' /work/Tools/smr/data/DICE/chr1.$j.qtltools > /work/Tools/smr/data/DICE/$j.qtltools
 for i in {2..22}
 do
  awk 'NR>1'  /work/Tools/smr/data/DICE/chr$i.$j.qtltools >> /work/Tools/smr/data/DICE/$j.qtltools
 done
done

## sometimes biomart is offline. Re-run what hasn't worked
## sometimes biomart is offline. Re-run what hasn't worked
for j in TREG_NAIVE  TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE 
do
 echo $j
 for i in {1..22}
 do
  echo $i
  if [ -s /work/Tools/smr/data/DICE/chr$i.$j.all.txt ]
  then
    echo -n ""
  else
	echo   "missing"
  fi
 done 
done


#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 echo $j
 gzip -f /work/Tools/smr/data/DICE/$j.qtltools
 rm /work/Tools/smr/data/DICE/chr*.$j.qtltools
done



## create smr file
# we need frequencies for both the GWAS and eQTL variants. We do not have these. We can add them based on 1KG however we do not know how AT/CG are coded. 
# here we will set them all to 0 and see if smr works.
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 /work/Tools/smr/smr_Linux --eqtl-summary /work/Tools/smr/data/DICE/$j.qtltools.gz --qtltools-nominal-format --make-besd --out /work/Tools/smr/data/DICE/$j
done

## .esi. Columns are chromosome, SNP, genetic distance (can be any arbitary value since it will not be used in the SMR analysis), basepair position, the effect (coded) allele, the other allele and frequency of the effect allele.
# the Effect allele is the ALT allele. 

# create a manifest for the SNPs
#for j in Th17 nB TREG_NAIVE  TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 rm /work/Tools/smr/data/DICE/$j.variants
 for i in {1..22}
 do
  awk 'NR>1 && !x[$12, $14]++ {print $12, $14, $15, $16}' /work/Tools/smr/data/DICE/chr$i.$j.all.txt  >> /work/Tools/smr/data/DICE/$j.variants
 done | awk '!x[$0]++'  > /work/Tools/smr/data/DICE/$j.variants
 wc -l /work/Tools/smr/data/DICE/$j.variants
done
# these files have more lines than the .esi ones...

# frequency files from 1KG EUR
# create non-AT/CG files
for i in {1..22}
do
 echo $i
 awk 'NR>1 && $5!=0 && !( ( ($3=="A" && $4=="T") || ($3=="T" && $4=="A") ) && ( ($3=="C" && $4=="G") || ($3=="G" && $4=="C") ))' /work/Genomes/1KG/EUR/chr"$i"_EUR.frq > /work/Genomes/1KG/EUR/chr"$i"_EUR_nonATCG_smr.frq
done
cat /work/Genomes/1KG/EUR/chr*_EUR_nonATCG_smr.frq > /work/Genomes/1KG/EUR/EUR_nonATCG_smr.frq

# create new .esi
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 Rscript /work/Tools/smr/data/DICE/esi_update.R -v /work/Tools/smr/data/DICE/$j.variants -m /work/Genomes/1KG/EUR/EUR_nonATCG_smr.frq -e /work/Tools/smr/data/DICE/$j.esi  -o /work/Tools/smr/data/DICE/$j.esi.update 
done

# there might some duplicates lines; variants that for some reason have two entries...
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 cp /work/Tools/smr/data/DICE/$j.esi.update /work/Tools/smr/data/DICE/$j.esi.update.bak
 # we will keep only the first entry
 awk '!x[$2]++' /work/Tools/smr/data/DICE/$j.esi.update.bak > /work/Tools/smr/data/DICE/$j.esi.update
 # test n of lines
 if [[ `wc -l /work/Tools/smr/data/DICE/$j.esi | awk '{print $1}'` -eq `wc -l /work/Tools/smr/data/DICE/$j.esi.update | awk '{print $1}'` ]]
 then 
  echo "Same n of lines"
 else
  wc -l /work/Tools/smr/data/DICE/$j.esi
  wc -l /work/Tools/smr/data/DICE/$j.esi.update
 fi
done


# version without any NA for freq (e.g., AT/CG)

## .epi. Columns are chromosome, probe ID(can be the ID of an exon or a transcript for RNA-seq data), genetic distance (can be any arbitary value), physical position, gene ID and gene orientation
# the files are missing the gene name

# create a manifest for the genes
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 rm /work/Tools/smr/data/DICE/$j.genes
 for i in {1..22}
 do
  awk 'NR>1 && !x[$1]++ {print $3, $1}' /work/Tools/smr/data/DICE/chr$i.$j.all.txt  >> /work/Tools/smr/data/DICE/$j.genes
 done 
 wc -l /work/Tools/smr/data/DICE/$j.genes
done

# merge in R 
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 Rscript /work/Tools/smr/data/DICE/epi_update.R -g /work/Tools/smr/data/DICE/$j.genes -e /work/Tools/smr/data/DICE/$j.epi  -o /work/Tools/smr/data/DICE/$j.epi.update 
done

# create new .epi
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
/work/Tools/smr/smr_Linux --beqtl-summary /work/Tools/smr/data/DICE/$j --update-epi /work/Tools/smr/data/DICE/$j.epi.update
done

# update besd file
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 /work/Tools/smr/smr_Linux --beqtl-summary /work/Tools/smr/data/DICE/$j  --update-esi /work/Tools/smr/data/DICE/$j.esi.update
 /work/Tools/smr/smr_Linux --beqtl-summary /work/Tools/smr/data/DICE/$j  --update-epi /work/Tools/smr/data/DICE/$j.epi.update
done





# - cleanup
parallel -j10 --bar 'gzip /work/Tools/smr/data/DICE/chr{1}.{2}.all.txt' ::: {1..22} ::: Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE

parallel -j10 --bar 'rm /work/Tools/smr/data/DICE/chr{1}.{2}.qtltools' ::: {1..22} ::: Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE

parallel -j10 --bar 'rm /work/Tools/smr/data/DICE/chr{1}.{2}.tmp' ::: {1..22} ::: Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE



## - run MS GWAS - ##
## note: that we use a thershold of <1e-5 for eQTLs (default is 5e-8)
## we should probably prepare the list of gene/snp from the cis results to feed in the model.
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 echo $j
 /work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR  --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/DICE/$j --peqtl-smr 1e-5 --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_$j  --thread-num 30 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_$j.log
done

echo "Tissue N_genes N_significant" > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE.summary
for tissue in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 echo -n $tissue
 # get n of tested genes
 n=`awk 'NR>1 && $19!~"nan"' /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue".smr | grep -v "nan" | wc -l `
 echo -n " "$n" "
 sort -gk 19 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue".smr | awk -v n=$n '$19<(0.05/n) && $20>=0.01' |  grep -v "nan"  > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue".smr.top
 wc -l /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue".smr.top | awk '{print $1}'
done >> /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE.summary

## smr multi
#for j in Th17 nB TREG_NAIVE TREG_MEM THSTAR TH2 TH1 TFH
for j in NK MONOCYTES M2 CD8_STIM CD8_NAIVE CD4_STIM CD4_NAIVE
do
 /work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR   --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/DICE/$j --peqtl-smr 1e-5 --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$j"_multi --smr-multi  --thread-num 30 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$j"_multi.log
done

echo "Tissue N_genes N_significant" > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_multi.summary
for tissue in Th17 nB TREG_NAIVE TREG_MEM THSTAR
do
 echo -n $tissue
 # get n of tested genes
 n=`awk 'NR>1 && $20!="nan"' /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue"_multi.msmr | grep -v "nan" | wc -l `
 echo -n " "$n" "
 sort -gk 20 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue"_multi.msmr | awk -v n=$n '$20<(0.05/n) && $21>=0.01' |  grep -v "nan"  > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue"_multi.msmr.top
 wc -l /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_"$tissue"_multi.msmr.top | awk '{print $1}'
done >> /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_DICE_multi.summary

### -- ENDOF DICE -- ###



Notes:
2. 1KG: create bld files






### -- immunexut -- ###
## the LD pattern is different
## we are going to format so these are ready for coloc and simple enrichment
## all results are here: /archive/data/immunexut/nominal
## available columns: Gene_id	Gene_name	CHR	TSS_position	Number_of_variants_cis	Variant_ID	REF	ALT	Variant_CHR	Variant_position_start	Variant_position_end	nominal_P_value	slope(ALT)
## we will create the following columns: ensemble_id gene_CHR gene_start_hg19 strand n_cis distance_hg19 Variant variant_CHR POS_hg19 p_eqtl beta_eqtl top_cis
## we do not have the strand. We can add this in R
## move to dir where we store SMR formatted data

cd /work/Tools/smr/data
mkdir immunexut
cd immunexut

## split per chromosome, so we can parallelize processing. These files are smaller than the DICE ones. Are there variants filtered?
for i in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 DN_B SM_B USM_B Naive_B
do 
 echo $i
 zcat /archive/data/immunexut/nominal/"$i"_nominal.txt.gz | awk -v i=$i '{print $0 > $3"."i".tmp"}'  
done

## Prepare file in QTLtools format
# main file. This queries biomart and cannot be paralellized
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 echo $j
 for i in {1..22}
 do
  echo $i
   Rscript /work/Tools/smr/data/immunexut/SMR_format.R -i /work/Tools/smr/data/immunexut/chr$i.$j.tmp -o /work/Tools/smr/data/immunexut/chr$i.$j.qtltools -g /work/Tools/smr/data/immunexut/chr$i.$j.all.txt
 done
done

## sometimes biomart is offline. Re-run what hasn't worked
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 echo $j
 for i in {1..22}
 do
  echo $i
  if [ -s "/work/Tools/smr/data/immunexut/chr$i.$j.all.txt" ]
  then
    echo "Completed"
  else
   Rscript /work/Tools/smr/data/immunexut/SMR_format.R -i /work/Tools/smr/data/immunexut/chr$i.$j.tmp -o /work/Tools/smr/data/immunexut/chr$i.$j.qtltools -g /work/Tools/smr/data/immunexut/chr$i.$j.all.txt
  fi
 done 
done




# join all chromosomes in one file
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 awk 'NR>1' /work/Tools/smr/data/immunexut/chr1.$j.qtltools > /work/Tools/smr/data/immunexut/$j.qtltools
 for i in {2..22}
 do
  awk 'NR>1'  /work/Tools/smr/data/immunexut/chr$i.$j.qtltools >> /work/Tools/smr/data/immunexut/$j.qtltools
 done
done

## fix variants with more than one IDs. There are variants that have two values seperated by ";". We will keep the second one, which is usually an rsid
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 awk -F '[ ;]' '{
  if ( NF==15 ) print $1, $2, $3, $4, $5, $6, $7, $9, $10, $11, $12, $13, $14, $15;
  else print $0;
  }'  /work/Tools/smr/data/immunexut/$j.qtltools > /work/Tools/smr/data/immunexut/$j.qtltools.tmp
 mv /work/Tools/smr/data/immunexut/$j.qtltools.tmp /work/Tools/smr/data/immunexut/$j.qtltools
done

# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 echo $j
 gzip -f /work/Tools/smr/data/immunexut/$j.qtltools
 #rm /work/Tools/smr/data/immunexut/chr*.$j.qtltools
done



## create smr file
# we need frequencies for both the GWAS and eQTL variants. We do not have these. We can add them based on 1KG however we do not know how AT/CG are coded. 
# here we will set them all to 0 and see if smr works.
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 /work/Tools/smr/smr_Linux --eqtl-summary /work/Tools/smr/data/immunexut/$j.qtltools.gz --qtltools-nominal-format --make-besd --out /work/Tools/smr/data/immunexut/$j
done

## .esi. Columns are chromosome, SNP, genetic distance (can be any arbitary value since it will not be used in the SMR analysis), basepair position, the effect (coded) allele, the other allele and frequency of the effect allele.
# the Effect allele is the ALT allele. 

# create a manifest for the SNPs
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 rm /work/Tools/smr/data/immunexut/$j.variants
 for i in {1..22}
 do
  awk 'NR>1 && !x[$9, $12]++ {print $9, $12, $10, $11}'  /work/Tools/smr/data/immunexut/chr$i.$j.all.txt  |  awk -F '[ ;]' '{
  if ( NF==5 ) print $1, $3, $4, $5;
  else print $0;
  }'  
 done | awk '!x[$0]++'  > /work/Tools/smr/data/immunexut/$j.variants
 wc -l /work/Tools/smr/data/immunexut/$j.variants
done
# these files have more lines than the .esi ones...


# create new .esi
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 Rscript /work/Tools/smr/data/DICE/esi_update.R -v /work/Tools/smr/data/immunexut/$j.variants -m /work/Genomes/1KG/EUR/EUR_nonATCG_smr.frq -e /work/Tools/smr/data/immunexut/$j.esi  -o /work/Tools/smr/data/immunexut/$j.esi.update 
done

# there might some duplicates lines; variants that for some reason have two entries...
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 cp /work/Tools/smr/data/immunexut/$j.esi.update /work/Tools/smr/data/immunexut/$j.esi.update.bak
 # we will keep only the first entry
 awk '!x[$2]++' /work/Tools/smr/data/immunexut/$j.esi.update.bak > /work/Tools/smr/data/immunexut/$j.esi.update
 # test n of lines
 if [[ `wc -l /work/Tools/smr/data/immunexut/$j.esi | awk '{print $1}'` -eq `wc -l /work/Tools/smr/data/immunexut/$j.esi.update | awk '{print $1}'` ]]
 then 
  echo "Same n of lines"
 else
  wc -l /work/Tools/smr/data/immunexut/$j.esi
  wc -l /work/Tools/smr/data/immunexut/$j.esi.update
 fi
done


# version without any NA for freq (e.g., AT/CG)

## .epi. Columns are chromosome, probe ID(can be the ID of an exon or a transcript for RNA-seq data), genetic distance (can be any arbitary value), physical position, gene ID and gene orientation
# the files are missing the gene name

# create a manifest for the genes
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 rm /work/Tools/smr/data/immunexut/$j.genes
 for i in {1..22}
 do
  awk 'NR>1 && !x[$1]++ {print $1, $2}' /work/Tools/smr/data/immunexut/chr$i.$j.all.txt  >> /work/Tools/smr/data/immunexut/$j.genes
 done 
 wc -l /work/Tools/smr/data/immunexut/$j.genes
done

# merge in R 
#for j in Th17
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 Rscript /work/Tools/smr/data/DICE/epi_update.R -g /work/Tools/smr/data/immunexut/$j.genes -e /work/Tools/smr/data/immunexut/$j.epi  -o /work/Tools/smr/data/immunexut/$j.epi.update 
done

# create new .epi
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
/work/Tools/smr/smr_Linux --beqtl-summary /work/Tools/smr/data/immunexut/$j --update-epi /work/Tools/smr/data/immunexut/$j.epi.update
done

# update besd file
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 /work/Tools/smr/smr_Linux --beqtl-summary /work/Tools/smr/data/immunexut/$j  --update-esi /work/Tools/smr/data/immunexut/$j.esi.update
 /work/Tools/smr/smr_Linux --beqtl-summary /work/Tools/smr/data/immunexut/$j  --update-epi /work/Tools/smr/data/immunexut/$j.epi.update
done



## cleanup


## - run MS GWAS - ##
## note: that we use a thershold of <1e-5 for eQTLs (default is 5e-8)
## we should probably prepare the list of gene/snp from the cis results to feed in the model.
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 echo $j
 /work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR  --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/immunexut/$j --peqtl-smr 1e-5 --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_$j  --thread-num 30 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_$j.log
done

echo "Tissue N_genes N_significant" > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut.summary
for tissue in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do
 echo -n $tissue
 # get n of tested genes
 n=`awk 'NR>1 && $19!~"nan"' /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue".smr | grep -v "nan" | wc -l `
 echo -n " "$n" "
 sort -gk 19 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue".smr | awk -v n=$n '$19<(0.05/n) && $20>=0.01' |  grep -v "nan"  > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue".smr.top
 wc -l /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue".smr.top | awk '{print $1}'
done >> /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut.summary

## smr multi
# for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B
for j in  Tfh Mem_CD4 DN_B 
do
 /work/Tools/smr/smr_Linux --bfile /work/Genomes/1KG/EUR/EUR   --gwas-summary /work/Tools/smr/gwas/ms/discovery_metav3.0.smr.txt --beqtl-summary /work/Tools/smr/data/immunexut/$j --peqtl-smr 1e-5 --out /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$j"_multi --smr-multi  --thread-num 30 > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$j"_multi.log
done

echo "Tissue N_genes N_significant" > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_multi.summary
for tissue in Th17 
do
 echo -n $tissue
 # get n of tested genes
 n=`awk 'NR>1 && $20!="nan"' /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue"_multi.msmr | grep -v "nan" | wc -l `
 echo -n " "$n" "
 sort -gk 20 /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue"_multi.msmr | awk -v n=$n '$20<(0.05/n) && $21>=0.01' |  grep -v "nan"  > /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue"_multi.msmr.top
 wc -l /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_"$tissue"_multi.msmr.top | awk '{print $1}'
done >> /work/Projects/MS-chromatin_fine_mapping/results/smr/ms_immunexut_multi.summary


### -- ENDOF immunexut -- ###


