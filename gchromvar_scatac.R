#/project/jcreminslab/guomic_projects/software/R-3.6.3/bin/R
library(chromVAR)
library(gchromVAR)
library(BuenColors)
library(SummarizedExperiment)
library(data.table)
library(BiocParallel)
library(BSgenome.Hsapiens.UCSC.hg38)
library(BSgenome.Hsapiens.UCSC.hg19)
library(stringr)
library(scater)
library(rhdf5)
library(Seurat)

set.seed(123)
register(MulticoreParam(2))

#Read counts file
a<-H5Fopen("/Users/michaelguo/Dropbox/MS-chromatin_fine_mapping/data/scATAC/10xPBMC/atac_v1_pbmc_10k_filtered_peak_bc_matrix.h5")
SE<-readRDS("/project/jcreminslab/guomic_projects/ms/sc_atac/singlecell_bloodtraits/data/singlecell/scATAC/full_scHeme_QC.rds")
#SE<-readRDS("/cvar/jhlab/mguo/MS/scATAC/singlecell_bloodtraits/data/singlecell/scATAC/full_scHeme_QC.rds")
SE <- addGCBias(SE, genome = BSgenome.Hsapiens.UCSC.hg19)

#Read in fine-mapping results, posteriors
#https://personal.broadinstitute.org/mguo/MS.PICS.ld0.2.results.all.bed
ukbb<-importBedScore(rowRanges(SE), "/project/jcreminslab/guomic_projects/ms/sc_atac/gchromvar/MS.PICS.ld0.2.results.all.bed", colidx = 5)


#ukbb<-importBedScore(rowRanges(SE), "/project/jcreminslab/guomic_projects/ms/sc_atac/gchromvar/MS.susie_L1.gchromvar.bed", colidx = 5)
#ukbb <- importBedScore(rowRanges(SE), list.files("/cvar/jhlab/mguo/MS/PICS/run/", full.names = TRUE, pattern = ".bed$"))
str(ukbb)

#Run G-chromVAR
ukbb_wDEV <- computeWeightedDeviations(SE, ukbb)
zscoreRaw <- t(assays(ukbb_wDEV)[["z"]])
rownames(zscoreRaw) <- colData(SE)$name
colnames(zscoreRaw) <- gsub("_PP001", "", colnames(zscoreRaw))
mdf <- melt(zscoreRaw)
names(mdf)<-c("name", "pics", "Z")

#Merge with PCAs
pca<-read.delim("/project/jcreminslab/guomic_projects/ms/sc_atac/singlecell_bloodtraits/data/singlecell/scATAC/scHeme_meta.txt", header=T, stringsAsFactors = F, sep="\t")
#pca<-read.delim("/cvar/jhlab/mguo/MS/scATAC/singlecell_bloodtraits/data/singlecell/scATAC/scHeme_meta.txt", header=T, stringsAsFactors = F, sep="\t")
mdf.p<-merge(mdf, pca, by="name", all.x=F, all.y=F)
write.table(mdf.p, "scHeme_meta.MS.PICS.gchromVAR.txt", row.names = F, col.names = T, sep="\t", quote=F)


#Plot
#scp guomic@mercury.pmacs.upenn.edu:/project/jcreminslab/guomic_projects/ms/sc_atac/gchromvar/scHeme_meta.MS.Susie.gchromVAR.txt .
dat<-read.delim("/Users/michaelguo/Desktop/scHeme_meta.MS.PICS.gchromVAR.txt", header=T, sep="\t", stringsAsFactors = F)
#dat<-read.delim(url("https://personal.broadinstitute.org/mguo/scHeme_meta.MS.PICS.gchromVAR.txt"), header=T, sep="\t", stringsAsFactors = F)
par(mar=c(4,4,4,4), cex=1.0, cex.main=0.8, cex.axis=0.8)
#dat<-subset(dat,  pics=="MS.PICS.ld0.2.results.all")
#dat<-subset(dat, Z<2.5 & Z> -1.5)

require(RColorBrewer)
cols<-brewer.pal(12, "Set3")
dat.col<-data.frame(type=unique(dat$type), color=cols, stringsAsFactors = F)
dat<-merge(dat, dat.col, by="type", all.x=T, all.y=T)
#dat<-subset(dat, Z<2.5)
require(plotly)
#Plot by Z-score
p <- plot_ly(dat, x = ~PC1, y = ~PC2, z = ~PC3, marker = list(color = ~Z, colorscale = c('#FFE1A1', '#683531'), showscale=TRUE)) %>%
  add_markers() %>%
  layout(scene = list(xaxis = list(title = 'PC1'),yaxis = list(title = 'PC2'),zaxis = list(title = 'PC3')),
         annotations = list(x = 1.13,y = 1.05,text = 'Z-score',xref = 'paper', yref = 'paper', showarrow = FALSE))
p


#Plot by cell type
p <- plot_ly(dat, x = ~PC1, y = ~PC2, z = ~PC3,color=~type, colors=cols) %>%
  add_markers() %>%
  layout(scene = list(xaxis = list(title = 'PC1'),yaxis = list(title = 'PC2'),zaxis = list(title = 'PC3')),
         annotations = list(x = 1.13,y = 1.05,text = 'Z-score',xref = 'paper', yref = 'paper', showarrow = FALSE))
p


#Jitter plot
p = ggplot(dat, aes(x=type, y=Z)) +
  geom_point(aes(fill=type), size=2, shape=21, colour="grey20",
             position=position_jitter(width=0.2, height=0.1)) +
  geom_boxplot(outlier.colour=NA, fill=NA, colour="grey20") 
p