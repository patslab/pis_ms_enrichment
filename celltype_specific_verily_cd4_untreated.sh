#!/usr/bin/bash

### this script is written for Thalia

##LDSC tool setup
ldsc="/work/Tools/ldsc"
source activate ldsc

## hardwired variables
bedtools="/work/Tools/bedtools2/bin/bedtools"
bed_files_dir="/work/Projects/LDSC_Analysis/Verily_Temp/bed_files"
ldscores=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDscores

## move to working directory
cd /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/



#### general notes on the process. 
# all comparison will be pairwise, e.g. within the untreated T4nv vs T4cm, for each group
# for the control peak set we will collapse the peaks from both tested cells. 
# a sensitivity analysis will use all cells (T4 and B) to generate the control set for each group
# a third analysis will compare directly the per cell LD scores, i.e. not includinig a control set.


#### loop over the groups
## array with categories
groups=("untreated" "glatiramer" "interferon" "natalizumab")

## array with all cells
cells=(`cat B.txt CD4.txt | tr "\n" " " `)

## loop
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 
 ## start the nested loop
 for(( j=0; j<=$((${#cells[@]}-1)); j++))
 do
  k_init=$(( $j + 1 ))
  for(( k=$k_init; k<=$((${#cells[@]}-1)); k++))
  do 
   echo "Cell 1:" ${cells[$j]} "; Cell 2:"${cells[$k]}
   ## here we call the script to create the ld scores. We will not send this to the background, given that LDSC uses all threads.
   # set variables
   file1=${cells[$j]}"_"${groups[$i]}.merge.bed.gz
   file2=${cells[$k]}"_"${groups[$i]}.merge.bed.gz
   beddir=$bed_files_dir
   lddir=$ldscores
   outname=${cells[$j]}.${cells[$k]}.${groups[$i]}.control
   sh /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/prepare_LDscores_control.sh $file1 $file2 $beddir $lddir $outname 

  done
 done
 
done

  
### Make Ldcts file. These files will have tthe two cells vs the "common control".
mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/ldcts

## loop
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 
 ## start the nested loop
 for(( j=0; j<=$((${#cells[@]}-1)); j++))
 do
  k_init=$(( $j + 1 ))
  for(( k=$k_init; k<=$((${#cells[@]}-1)); k++))
  do 
   echo "Cell 1:" ${cells[$j]} "; Cell 2:"${cells[$k]}
   echo -e "$ldscores/${cells[$j]}"_"${groups[$i]}.,$ldscores/${cells[$j]}.${cells[$k]}.${groups[$i]}.control." > ldcts/${cells[$j]}.${cells[$k]}.${groups[$i]}.control.ldcts
   echo -e "$ldscores/${cells[$k]}"_"${groups[$i]}.,$ldscores/${cells[$j]}.${cells[$k]}.${groups[$i]}.control." >> ldcts/${cells[$j]}.${cells[$k]}.${groups[$i]}.control.ldcts

  done
 done
done 



##################################################################################################################################
##Run cell type specific model

GWAS="/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz"
ldsc_files_dir="/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/"
out_dir="/work/Projects/LDSC_Analysis/Verily_Temp/test_dir/"
ldcts_file="/work/Projects/LDSC_Analysis/Verily_Temp/test_dir/verily.ldcts"

python $ldsc/ldsc.py \
    --h2-cts $GWAS \
    --ref-ld-chr $ldsc_files_dir/baseline. \
    --out $out_dir/MS \
    --ref-ld-chr-cts $ldcts_file \
    --w-ld-chr $ldsc_files_dir/weights.


