
ldscores=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDscores/
## setup the environment
source activate ldsc
ldsc=/work/Tools/ldsc/ldsc.py

cd /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/

## array with categories
groups=("untreated" "glatiramer" "interferon" "natalizumab")
## create sudirectories
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Pairwise/${groups[$i]}

done


# how many cells in the file?
n=`cat CD4.txt B.txt | wc -l  | awk '{print $1}'`
# create an array with the cells
array=(`cat CD4.txt B.txt | tr '\n' ' '  `)

## loop over groups
for(( g=0; g<=$((${#groups[@]}-1)); g++))
do
 echo $g": "${groups[$g]}
 dir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Pairwise/${groups[$g]}
 
 ## create the CTS files. These files willl be one cell vs. each other, one in every row
 # First loop
 for(( i=0; i<${#array[*]}; i++ ))
 do
 echo "First: "${array[$i]}
 # start empty file 
 echo -n "" > $dir/${array[$i]}.cts
 # second loop
 for(( j=0; j<${#array[*]}; j++ ))
 do
  # test that it is not the same cell
  if(( $i != $j ))
  then
   echo "Second: "${array[$j]} 
   # cells 
   cells=`echo $ldscores"${array[$i]}""_"${groups[$g]}.,$ldscores"${array[$j]}""_"${groups[$g]}"."`
   echo ${array[$i]}"_"${array[$j]}" "$cells >> $dir/${array[$i]}.cts
  fi
  done
  # LDSC with baseline using cts module
  (python $ldsc \
  --h2-cts /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
  --ref-ld-chr-cts $dir/${array[$i]}.cts \
  --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline. \
  --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
  --out $dir/${array[$i]} &)
 done
done



