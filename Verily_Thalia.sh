### Update 6/16/2020: added pairwise comparisons (using stratified model)
### Update 7//7/2020: added single-subject untreated analyses and comparisons with treated subjects
### Update 8/14/2020: added baseline + annotation per annotation, i.e. one each time. This will be tthe primary analysis to provide the enrichment p-value

ldscores=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDscores/


mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified
mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Cell_specific

## setup the environment
source activate ldsc
ldsc=/work/Tools/ldsc/ldsc.py

cd /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/

## Note: we are going to keep only common cells between all 4 groups

# list of CD4_ cells
echo "T4nv" > CD4.txt
echo "T4cm" >> CD4.txt
echo "T4em" >> CD4.txt
echo "T4ra" >> CD4.txt
#echo "Treg" >> CD4.txt


# list of naive B cells
#echo "BnUS" > naive_B.txt
#echo "BnCS" >> naive_B.txt
echo "traB" > naive_B.txt

# list of effector B cells
echo "cMBc" > effector_B.txt
#echo "MBim" >> effector_B.txt
#echo "pBcs" >> effector_B.txt

# list of B cells
cat naive_B.txt > B.txt
cat effector_B.txt >> B.txt
 

## array with categories
groups=("untreated" "glatiramer" "interferon" "natalizumab")
## create sudirectories
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/${groups[$i]}

done


### one annotation each time + baseline (added in 8/14/2020)
## loop over groups
groups=("untreated" "glatiramer" "interferon" "natalizumab")
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/main/${groups[$i]}
 ## loop over cells
 cat CD4.txt B.txt | while read cell
 do
  echo $cell
  # with baseline
 (python $ldsc \
  --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
  --overlap-annot \
  --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,$ldscores/$cell"_"${groups[$i]}"." \
  --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
  --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
  --print-coefficients \
  --out /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/main/${groups[$i]}/baseline_"$cell" ) 

 done
done

## qvalue estimation per analysis
groups=("untreated" "glatiramer" "interferon" "natalizumab")
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/main/${groups[$i]}
 ## loop over cells
 cat CD4.txt B.txt | while read cell
 do
  echo $cell
  # format file

  # R script

  
 done
done

## aggregate results: keep only the lines of interest from all models in one file
echo "Category Prop._SNPs Prop._h2 Prop._h2_std_error Enrichment Enrichment_std_error Enrichment_p Coefficient Coefficient_std_error Coefficient_z-score" > results/main/summary.results
grep "_1" results/main/*/*results  | sed -e 's/^.*\://' -e 's/\t/ /g' >> results/main/summary.results








### Stratified models. REPEAT T cells
## loop over groups
groups=("untreated" "glatiramer" "interferon" "natalizumab")

for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 dir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/${groups[$i]}

 ## CD4 T and B

 # create directory
 mkdir -p $dir/CD4_B
 # cells
 cells=(`cat B.txt CD4.txt | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
  
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/CD4_B/stratified &)
 # no baseline
 ( python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr `echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/CD4_B/stratified_noBaseline &)

 ## T cells
 mkdir -p $dir/CD4
 # cells
 cells=(`cat CD4.txt | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
  
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/CD4/stratified &)
 # no baseline
  (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr `echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/CD4/stratified_noBaseline &)


## B cells
 mkdir -p $dir/B
 # cells
 cells=(`cat B.txt | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
  
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/B/stratified &)
 # no baseline
  (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr `echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/B/stratified_noBaseline &)


## Naive B cells
 mkdir -p $dir/naive_B
 # cells
 cells=(`cat naive_B.txt | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
  
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/naive_B/stratified &)
 # no baseline
  (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr `echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/naive_B/stratified_noBaseline &)

## Effector B cells
 mkdir -p $dir/effector_B
 # cells
 cells=(`cat effector_B.txt | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
  
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/effector_B/stratified &)
 # no baseline
  (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr `echo ${cells[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $dir/effector_B/stratified_noBaseline &)


done

for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 awk '{print $1, $7, $NF}' results/Stratified/${groups[$i]}/CD4_B/stratified.results  | sort -gk 3
done




for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 awk '{print $1, $7, $NF}' results/Stratified/${groups[$i]}/CD4/stratified.results  | sort -gk 3
done


for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 awk '{print $1, $7, $NF}' results/Stratified/${groups[$i]}/B/stratified.results  | sort -gk 3
done




#### per cell type, across groups
cat CD4.txt B.txt | while read cell
do
 echo $cell
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/$cell
  # with baseline
 (python $ldsc \
  --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
  --overlap-annot \
  --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,$ldscores$cell"_untreated.,"$ldscores$cell"_glatiramer.,"$ldscores$cell"_interferon.,"$ldscores$cell"_natalizumab." \
  --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
  --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
  --print-coefficients \
  --out /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/$cell/cross_group_Baseline &) 

done

## review results
cat CD4.txt B.txt | while read cell
do
 echo $cell
 awk '{print $1, $7, $NF}'  results/Stratified/$cell/cross_group_Baseline.results | sort -gk 3
done

### no untreated
cat CD4.txt B.txt | while read cell
do
 echo $cell
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/$cell
# baseline
 (python $ldsc \
  --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
  --overlap-annot \
  --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,$ldscores$cell"_glatiramer.,"$ldscores$cell"_interferon.,"$ldscores$cell"_natalizumab." \
  --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
  --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
  --print-coefficients \
  --out /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/$cell/cross_group_No_untreated_Baseline &) 



done


## review results
cat CD4.txt B.txt | while read cell
do
 echo $cell
 awk '{print $1, $7, $NF}'  results/Stratified/$cell/cross_group_No_untreated_Baseline.results | sort -gk 3
done

##### Pairwise
# how many cells in the file?
n=`cat CD4.txt B.txt | wc -l  | awk '{print $1}'`
# create an array with the cells
array=(`cat CD4.txt B.txt | tr '\n' ' '  `)


for(( g=0; g<=$((${#groups[@]}-1)); g++))
do
 echo $g": "${groups[$g]}
 dir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Pairwise/${groups[$g]}
 # First loop
 for(( i=0; i<$(( ${#array[*]} - 1 )); i++ ))
 do
  echo "First: "${array[$i]}
  # second loop
  for(( j=$(( $i + 1 )); j<${#array[*]}; j++ ))
  do
   echo "Second: "${array[$j]} 
   # cells 
   cells=`echo $ldscores"${array[$i]}""_"${groups[$g]}.,"$ldscores"${array[$j]}"_"${groups[$g]}"."`

   # with baseline and S-LDSC
  (python $ldsc \
  --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
  --overlap-annot \
  --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${cells[*]}` \
  --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
  --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
  --print-coefficients \
  --out $dir/"${array[$i]}"_"${array[$j]}" &)
  done
 done
done





## B cells; there only 2 subsets, so this equals already to pairwise comparisons



### across groups, within the same cell



### -- Added 7/7/2020 -- ##

cd /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/

### Untreated individual files and stratified analyses with treated within each cell type
dir=/work/Projects/LDSC_Analysis/Verily_Untreated/LDScores_individual


## individual untreated subjects' results
outdir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/untreated_ind/

mkdir -p $outdir
# loop over the cells
cat CD4.txt B.txt | while read cell
do
 echo $cell
 # loop over the manifest file
 manifest=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/manifests/untreated_manifests/utilized_May_2020_Untreated.$cell.list
 cat $manifest | while read line
 do
  # transform to an array
  line2=(`echo $line | sed -e 's/\,/ /g' | sed -e 's/"//g' `)
  #echo ${line2[0]}
  # Ld scores path
  path=$dir/${line2[2]}.$cell.
  # output directory
  mkdir -p $outdir/${line2[3]}
  # with baseline
  (python $ldsc \
  --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
  --overlap-annot \
  --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo $path` \
  --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
  --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
  --print-coefficients \
  --out $outdir/${line2[3]}/$cell &)

 done 
done

## collect the results
# Baseline

# No Baseline



## stratified, all individuals untreated
outdir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/untreated_ind/
cat CD4.txt B.txt | while read cell
do
 echo $cell
 # create subdir 
 mkdir -p $outdir/$cell
 # loop over the manifest file
 manifest=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/manifests/untreated_manifests/utilized_May_2020_Untreated.$cell.list
 files=$(cat $manifest | while read line
 do
  # transform to an array
  line2=(`echo $line | sed -e 's/\,/ /g' | sed -e 's/"//g' `)
  #echo ${line2[0]}
  # Ld scores path
  echo $dir/${line2[2]}.$cell.
 done | tr '\n' ',' | sed -s 's/\,$//')
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${files[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $outdir/$cell/baseline &)


done


### Stratified, all individuals untreated + treated
ldscores=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDscores

outdir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/untreated_ind
cat CD4.txt B.txt | while read cell
do
 echo $cell
 # create subdir 
 mkdir -p $outdir/$cell
 # loop over the manifest file
 manifest=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/manifests/untreated_manifests/utilized_May_2020_Untreated.$cell.list
 files=$(cat $manifest | while read line
 do
  # transform to an array
  line2=(`echo $line | sed -e 's/\,/ /g' | sed -e 's/"//g' `)
  #echo ${line2[0]}
  # Ld scores path
  echo $dir/${line2[2]}.$cell.
 done | tr '\n' ',' | sed -s 's/\,$//')
 files=`echo $files","$ldscores/$cell"_glatiramer.,"$ldscores/$cell"_interferon.,"$ldscores/$cell"_natalizumab."`
 # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${files[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $outdir/$cell/Untreated_treated_baseline &)

done

## review results with baseline model
cat CD4.txt B.txt | while read cell
do
 echo $cell
 awk -v cell=$cell 'NR==1 || $1~cell' results/Stratified/untreated_ind/$cell/Untreated_treated_baseline.results

done

### all cells stratified within individual
dir=/work/Projects/LDSC_Analysis/Verily_Untreated/LDScores_individual
manifest=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/manifests/utilized_May_2020_Untreated.list
outdir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Stratified/untreated_ind

# loop over the subjects
awk -F, '!x[$4]++ {print $4}' $manifest | sed -e 's/\"//g' | while read subject
do
 echo $subject
 mkdir -p $outdir/$subject
 files=$(grep $subject manifests/untreated_manifests/utilized_May_2020_Untreated.*.list | while read line
 do
  # transform to an array
  line2=(`echo $line | sed -e 's/\,/ /g' | sed -e 's/"//g' `)
  #echo ${line2[0]}
  # Ld scores path
  echo $dir/${line2[2]}.${line2[1]}.
 done | tr '\n' ',' | sed -s 's/\,$//')
  # with baseline
 (python $ldsc \
 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
 --overlap-annot \
 --ref-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/baseline.,`echo ${files[*]}` \
 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
 --print-coefficients \
 --out $outdir/$subject/baseline &)

done


