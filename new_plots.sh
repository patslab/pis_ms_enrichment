#### Code to generate plots add in the revision.





#### Dot plot for coloc/OCR+loops enrichment
R --no-restore --no-save
## load overlap summary file
data <- read.table("/work/Projects/MS-chromatin_fine_mapping/results/coloc/DICE/overlap_summary.txt", header=TRUE)
## order the cells
data$Cell <- factor(data$Cell,levels = c("TH1", "TH2", "TFH", "Th17", "THSTAR", "TREG_NAIVE", "TREG_MEM", "CD4_NAIVE", "CD4_STIM", "nB"))

## dot plot
library(ggplot2)
p <- ggplot(data, aes(x=Cell, y=-log10(H4_60per)))+
  geom_point(shape=1)+
  coord_flip()+theme_bw()+geom_hline(yintercept = -log10(0.05/nrow(data)),linetype="dashed") + ylab("-log10 p-value of colocalization enrichment")
pdf("/work/Projects/MS-chromatin_fine_mapping/results/coloc/DICE/coloc_overlap_enrichment.pdf")
print(p)
dev.off()
