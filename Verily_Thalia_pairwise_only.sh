## path with ldscores
ldscores=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDscores/



## setup the environment
source activate ldsc
ldsc=/work/Tools/ldsc/ldsc.py

cd /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/

## Note: This script assume that all files are in place, i.e. the Verily_Thalia.sh was already run.

## array with categories
groups=("untreated" "glatiramer" "interferon" "natalizumab")
## create sudirectories
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 mkdir -p /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Pairwise/${groups[$i]}

done


### Pairwise models within groups
## loop over groups
for(( i=0; i<=$((${#groups[@]}-1)); i++))
do
 echo $i": "${groups[$i]}
 dir=/work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/results/Pairwise/${groups[$i]}

 # cells
 cells=(`cat B.txt CD4.txt`)
 # loop over the cells
 for((j=0; j<=${#cells[*]}; j++ ))
 do
  cell1=${cells[$j]}
  #echo $cell1
  # nested loop
  for((k=$(($j+1)); k<${#cells[*]}; k++ ))
  do
   cell2=${cells[$k]}
   echo $cell1" "$cell2	
   cell1_ld=(` cat B.txt CD4.txt | grep $cell1 | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
   cell2_ld=(` cat B.txt CD4.txt | grep $cell2 | awk -v ldscores=$ldscores -v group=${groups[$i]} '{print ldscores$0"_"group".,"}' |  tr '\n' ' ' | sed -e 's/\,\s\+$//' | sed  -e 's/\,\s\+/\,/g' `)
   
   #echo $cell1_ld","$cell2_ld
    # run LDSC. We will not run this in the background, because it will saturated the threads
     ( python $ldsc \
	 --h2 /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/MS.sumstats.gz \
	 --overlap-annot \
	 --ref-ld-chr `echo $cell1_ld","$cell2_ld` \
	 --w-ld-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/weights. \
	 --frqfile-chr /work/Projects/LDSC_Analysis/PID_chromatin_mapping_Mike/LDSC_files/1000G.mac5eur. \
	 --print-coefficients \
	 --out $dir/$cell1.$cell2 )

  done
 done 
done  


### Pairwise model within cells

