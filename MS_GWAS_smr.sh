#### Prepare MS GWAS (Science 2019) for smr

## working dir
cd /work/Tools/smr/gwas/ms
conda activate smr

# we want the following format: SNP    A1  A2  freq    b   se  p   n
# n is the sample size: 41505 for N=15 in the file


## format file in R
R --no-restore --no-save

# - libraries - #
library(SNPlocs.Hsapiens.dbSNP144.GRCh37)
library(GenomicRanges)
library(AllelicImbalance)


# - load data -#
data <- read.table("discovery_metav3.0.meta", header=TRUE)
nrow(data) # 8589719

# - keep only N==15 - #
data <- data[data$N==15, ]
nrow(data) # 6773531

# - remove lines with NAs - #
data <- data[!is.na(data$P),]
nrow(data) # 6773482

# - replace p values == 0 - #
data$P[data$P==0] <- 1e-310

# - beta - #
data$b <- log(data$OR)

# - z score -#
# add z score
data$z <- abs(qnorm( data$P/2, lower.tail=FALSE ))
# add directionality to z scores
data$z[data$b<0] <- -data$z[data$b<0] 

# - se - #
data$se <- data$b/data$z
data <- data[!is.na(data$se),]
nrow(data) # 6773482

# - sample size - #
data$n <- 41505


# - format output file - #
data <- data[ , c(3, 4, 5, 13, 9, 11, 7, 12)]
names(data)[7] <- "p"


# - add frequencies (US2 WTCCC2 data set, post imputation; same order of alleles) - #
data$freq <- "NA"
# read file with post-imputation frequencies
freq <- read.table(gzfile("/archive/MS/Broad_archive/pca5.newsnp.assoc.dosage.pos.SEcor.txt.gz"), header=TRUE)
# trim
freq <- freq[ , c(1, 5:7)]
# rename alleles, so that we can later do a check
names(freq)[2:3] <- c("A1_fr", "A2_fr")
# merge
all <- merge(data, freq, all.x=TRUE)
nrow(data)
nrow(all)
nrow(all[ all$A1==all$A1_fr, ] ) # all OK!

# - Add rsIDs - #
# create a GRanges object
gr_in <- makeGRangesFromDataFrame(all, keep.extra.columns=TRUE, seqnames.field="CHR", start.field="BP",end.field="BP" )
seqlevelsStyle(gr_in) <- "NCBI"
gr_in2 <- getSnpIdFromLocation(gr_in, SNPlocs.Hsapiens.dbSNP144.GRCh37)
# add SNPs in first granges object. Add and an if else test to compare length of objects(?)
gr_in$SNP_new <- names(gr_in2)
all2 <- as.data.frame(gr_in,  row.names=NULL)
nrow(all2) # 6773153!
# sanity checks. We expect many to have the same ID
nrow(all2[ all2$SNP==all2$SNP_new, ] ) #5619267
nrow(all2[ all2$SNP!=all2$SNP_new, ] ) #1201252

# the SNP_new has some NAs. So, we will populate the SNP name from the SNP col
all2$SNP_new[is.na(all2$SNP_new)] <-  all2$SNP[is.na(all2$SNP_new)]
summary(as.factor(all2$SNP_new)) # no more NAs


# - remove extra rows and format -# 
all2 <- all2[ , c(20, 7:8, 19, 12, 14, 10, 15)]
all <- all[, -c(5:6)]
# rename header
names(all2) <- c("SNP", "A1", "A2", "freq", "b", "se", "p", "n")



# - save file -#
write.table(all2, file="discovery_metav3.0.smr.txt", quote=FALSE, row.names=FALSE)

# - save version without AT/CG - #
all3 <- all2[ !( (all2$A1=="A" & all2$A2=="T") |  (all2$A1=="T" & all2$A2=="A") |  (all2$A1=="C" & all2$A2=="G") |  (all2$A1=="G" & all2$A2=="C") ) , ]
nrow(all3) # 5726168
write.table(all3, file="discovery_metav3.0.noATCG.smr.txt", quote=FALSE, row.names=FALSE)

q()


