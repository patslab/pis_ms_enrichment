#### Identification of gene sets for DICE and Immunexut results
### the question here are which eGenes are unique for each cell type. This sounds easy but it can be tricky, given that it is an exercise relative to the cell types at hand.
### there are two key parameters: i) cells that are part of the comparison, ii) definition of unique.


### -- DICE -- ###

## - Definition of unique for DICE - ##
## liberal approach: passed FDR of 5% (what threshold has Mike used?) in one cell type but not the other(s). 
 

## conservative approach: passed  FDR of 5%(?) in one cell type and p-value for any SNP in eGene is >0.05. 


## - ENDOF Definition of unique for DICE - ##


### -- ENDOF DICE -- ###



### -- immunexut -- ###
cd  /work/Tools/MESC/data/immunexut

## - Definition of unique for immunexut - ##
### liberal approach: eGenes passed FDR of 5% (what threshold has Mike used?) in one cell type but not the other(s). 
## how many genes have at least one variant that passed FDR of 5%?
for i in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do 
 echo -n $i" "
 awk '$10==0 && !x[$2]++'  /archive/data/immunexut/conditional/"$i"_conditional_eQTL_FDR0.05.txt | wc -l
done
#Th17 7550
#Fr_II_eTreg 7075
#Fr_I_nTreg 5465
#Th2 7288
#Th1 7112
#Naive_CD4 7360
#Fr_III_T 6686
#SM_B 7724
#USM_B 7094
#Naive_B 7055
#Tfh 7687
#Mem_CD4 7592
#DN_B 6952

## lists of eGenes
for i in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do 
 echo $i
 awk 'NR==1 || ($10==0 && !x[$2]++) {print $1, $2, $3, $4, $5}'  /archive/data/immunexut/conditional/"$i"_conditional_eQTL_FDR0.05.txt > $i.eGenes
done

## present in any CD4 T cells
for i in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 
do
 awk 'NR>1 {print $1, $2, $3}' $i.eGenes
done  | awk '!x[$0]++' > CD4.eGenes
wc -l CD4.eGenes
# 13520 CD4.eGenes


## present in any B cells
for i in SM_B USM_B Naive_B DN_B
do 
 awk 'NR>1 {print $1, $2, $3}' $i.eGenes
done  | awk '!x[$0]++' > B.eGenes
wc -l B.eGenes
# 11031 B.eGenes


## present in  any CD4 T and any B cells
cat CD4.eGenes B.eGenes |  awk '!x[$0]++' > CD4_B.eGenes
wc -l CD4_B.eGenes
#15717 CD4_B.eGenes

## per CD4 "unique" gene set. This needs to be present in the cell type of interest, not in the other CD4 T and not in B cells. This step will be faster in R
for i in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 
do
 for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 
 do
  # check if it is the same cell type
  if [[ $i == $j ]]
  then
   echo $i "is the same as "$j
  else
   awk 'NR>1 {print $1, $2, $3}' $j.eGenes
  fi
 done |  awk '!x[$0]++' > tmp.egenes
 
 # eGenes in cell of interest but not the other CD4
 grep -v -f tmp.egenes $i.eGenes > $i.un_eGenes
 #  eGenes in cell of interest but not in B
 grep -v -f B.eGenes $i.un_eGenes > $i.un_nonB_eGenes
 rm tmp.egenes

done

## per B "unique" gene set. This needs to be present in the cell type of interest, not in the other B and not in CD4 T cells. This step will be faster in R
for i in SM_B USM_B Naive_B DN_B 
do
 for j in SM_B USM_B Naive_B DN_B 
 do
  # check if it is the same cell type
  if [[ $i == $j ]]
  then
   echo $i "is the same as "$j
  else
   awk 'NR>1 {print $1, $2, $3}' $j.eGenes
  fi
 done |  awk '!x[$0]++' > tmp.egenes
 
 # eGenes in cell of interest but not the other B
 grep -v -f tmp.egenes $i.eGenes > $i.un_eGenes
 #  eGenes in cell of interest but not in CD4 T
 grep -v -f CD4.eGenes $i.un_eGenes > $i.un_nonCD4_eGenes
 rm tmp.egenes
done


### conservative approach: passed  FDR of 5%(?) in one cell type and p-value for any SNP in eGene is >0.05. 


## - ENDOF Definition of unique for immunexut - ##





### -- ENDOF immunexut -- ###

