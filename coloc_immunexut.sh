### Prepare immunexut eQTL results for coloc with MS GWAS results
## list of effects: we need to make a list of GWAS effects that overlaps OCRs+PCHiC. We can do this per cell type?
## this might be tricky, so we can start by creating a list for the 200 effects or maybe only the ones that the maringal effect was ~ as the conditional. This will result in 200 or less set of files. 
## now for each, we need to identify the region. We can simplify this by adding +-10K (?) around the range of the PICS variantscfor each effect. 
## then, we will have list of regions with the respective top variant.
## for each of these, we can extract the respective regions from the eQTLs. Now, the tricky part is that these might be many genes per cell type. We need to find a rule here...
## this gene list also needs to overlap with the PCHiC per cell type.


  
## we need files with the summary statistics (per chromosome is inf eand can speedup the operations) and the effects (i.e., the top variant per gene)
## However, we do not have MAFs. There are few options: i) drop all AT/CG SNPs and take MAF from 1KG, ii) set sdY to 1
## We have already processed files for the SMR analyses and we can reuse most of the files



## -- setup system
mkdir -p  /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut
cd /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut

screen -S coloc
conda activate smr

## -- step 1. Find overlapping variants
## There is detailed list with any PICS or SNP in LD (r2>0.2). We will use this list to create two set of files: i) complete: these files will capture eGenes that their cis-eQTL genes overlap with at least one SNP. ii) refined: these files will subset the eGenes from the complete files so that the GWAS variants will also overlapp OCRs+PCHiCHs for any(?) cell type. The provided nominal results do not have FDR or something similar. We will allow any eGene to proceed that has at least one p-value<10-5. 
mkdir data_prep
#parallel -j30 --bar 'Rscript /work/Projects/MS-chromatin_fine_mapping/scripts/immunexut_overlap.R -v /work/Projects/MS-chromatin_fine_mapping/results/MS.PICS.ld0.2.ATAC.gencode_v19.PCHiC.motifbreakr.eqtl.cs.bed -e /work/Tools/smr/data/immunexut/chr{1}.{2}.all.txt -o /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/data_prep/{1}.{2}' ::: {1..22} ::: Th17 
parallel -j30 --bar 'Rscript /work/Projects/MS-chromatin_fine_mapping/scripts/immunexut_overlap.R -v /work/Projects/MS-chromatin_fine_mapping/results/MS.PICS.ld0.2.ATAC.gencode_v19.PCHiC.motifbreakr.eqtl.cs.bed -e /work/Tools/smr/data/immunexut/chr{1}.{2}.all.txt -o /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/data_prep/{1}.{2}' ::: {1..22} :::  Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B


## -- step 2. Format file with summary statistics in R and create the effects (SNP-eGene) file
## we need the beta (lnOR) and var of beta, which is te square(^2) of the SE (of lnOR)
## min info: SNP POS beta var_beta N MAF quant/cc
## N is sample size and is used with MAF to estimate sdY for quantitative phenotypes. 
## However, we do not have MAF (for the author provided summary statistics) and it is a bit risky to guestimate the AT/CG SNPs. We are going to fix sdY to 1 and we can perform sensitivities analyses later on with various values.
#for i in Th17 
for i in Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do
 mkdir $i
 for j in {1..22}
 do
  mkdir $i/chr$j
 done
done
#parallel -j30 --bar 'Rscript /work/Projects/MS-chromatin_fine_mapping/scripts/immunexut_prepare_coloc.R -e /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/data_prep/{1}.{2}.complete.txt -o /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}/all.coloc.txt -d /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}' ::: {1..22} ::: Th17
parallel -j30 --bar 'Rscript /work/Projects/MS-chromatin_fine_mapping/scripts/immunexut_prepare_coloc.R -e /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/data_prep/{1}.{2}.complete.txt -o /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}/all.coloc.txt -d /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}' ::: {1..22} ::: Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B


## -- step 3. Prepare the per effect MS GWAS summary statistics. This can be done only once for all studies
## already done for DICE
## the output is one file per effect with the following columns: chr position snp A1 A2 OR beta se varbeta z p freq type

## -- Step 4. run coloc
#parallel -j30 --bar 'Rscript /work/Projects/MS-chromatin_fine_mapping/scripts/coloc_immunexut.R -l /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/data_prep/{1}.{2}.effects_genes.txt -v /work/Projects/MS-chromatin_fine_mapping/results/coloc/GWAS/MS/ -e /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}/ -o /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}/' ::: {1..22} ::: Th17
parallel -j30 --bar 'Rscript /work/Projects/MS-chromatin_fine_mapping/scripts/coloc_immunexut.R -l /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/data_prep/{1}.{2}.effects_genes.txt -v /work/Projects/MS-chromatin_fine_mapping/results/coloc/GWAS/MS/ -e /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}/ -o /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/{2}/chr{1}/' ::: {1..22} ::: Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B


## create a joined file
for j in  Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do
 awk 'NR==1 {print "chr", $0}' /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/$j/chr1/coloc_res.txt > /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/$j/coloc_summary.txt
 for i in {1..22}
 do
  awk -v chr=chr$i 'NR>1 {print chr, $0}' /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/$j/chr$i/coloc_res.txt
 done >> /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/$j/coloc_summary.txt
done   

for j in  Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do
 echo -n $j" "
 awk 'NR>1 && $NF!="NA" && $NF>0.8' /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/$j/coloc_summary.txt | wc -l 
done  


## compress
cd /work/Projects/MS-chromatin_fine_mapping/results/coloc/immunexut/
for j in  Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T SM_B USM_B Naive_B Tfh Mem_CD4 DN_B
do
 echo -n 
 tar -czvf $j.tar.gz $j/coloc_summary.txt
done





