#### Information on how to process immunexut results for MESC


### -- Th17 -- ##

screen -S MESC
#conda create -n mesc python=2.7 R=4.1
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Th17_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Th17.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Th17.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Th17.all.txt & done



## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt.*

done 


#awk '!x[$1, $2]++ {print $1, $2}' Th17.hg19.txt >  Th17.hg19.txt.tmp
#cp Th17.hg19.txt Th17.hg19.txt.tmp2
#cat Th17.hg19.txt.tmp | awk 'x[$1]++ {print $1}' | sort -u | while read line
#do
# grep -v $line Th17.hg19.txt.tmp2 > Th17.hg19.txt.tmp3
# cp Th17.hg19.txt.tmp3 Th17.hg19.txt.tmp2
#done
#cp Th17.hg19.txt.tmp2 Th17.hg19.txt
#rm Th17.hg19.txt.tmp*


## estimate gene expression scores
mkdir  /work/Tools/MESC/mesc/data/immunexut/
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Th17.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Th17
done

python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Th17 --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Th17.MS


### -- ENDOF Th17 -- ##


### -- DN_B -- ##
# Double Negative B cells: DN_B_nominal.txt.gz

screen -S DN_B
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/DN_B_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".DN_B.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.DN_B.tmp -o /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.DN_B.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.DN_B.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.DN_B 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.DN_B --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/DN_B

### -- ENDOF DN_B -- ##


### -- Mem_CD4 -- ##
# Memory CD4 T cells: Mem_CD4_nominal.txt.gz

screen -S Mem_CD4
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Mem_CD4_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Mem_CD4.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Mem_CD4.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Mem_CD4 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Mem_CD4 --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Mem_CD4

### -- ENDOF Mem_CD4 -- ##



### -- Tfh -- ##
# T follicular helper cells: Tfh_nominal.txt.gz

screen -S Tfh
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Tfh_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Tfh.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Tfh.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Tfh.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Tfh.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Tfh 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Tfh --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Tfh

### -- ENDOF Tfh -- ##




### -- Fr_III_T -- ##
# Fraction III non-regulatory T cells: Fr_III_T_nominal.txt.gz

screen -S Fr_III_T
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Fr_III_T_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Fr_III_T.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Fr_III_T.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Fr_III_T 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Fr_III_T --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Fr_III_T

### -- ENDOF Fr_III_T -- ##


### -- Naive_CD4 -- ##
# Naïve CD4 T cells: Naive_CD4_nominal.txt.gz

screen -S Naive_CD4
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Naive_CD4_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Naive_CD4.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Naive_CD4.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Naive_CD4 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Naive_CD4 --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Naive_CD4

### -- ENDOF Naive_CD4 -- ##


### -- Th2 -- ##
# T helper 2 cells: Th2_nominal.txt.gz

screen -S Th2
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Th2_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Th2.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Th2.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Th2.all.txt
done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Th2.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Th2 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Th2 --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Th2

### -- ENDOF Th2 -- ##


### -- Th1 -- ##
# T helper 1 cells: Th1_nominal.txt.gz

screen -S Th1
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Th1_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Th1.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Th1.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Th1.all.txt
done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Th1.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Th1 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Th1 --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Th1

### -- ENDOF Th1 -- ##


### -- Fr_I_nTreg -- ##
# Fraction I naïve regulatory T cells: Fr_I_nTreg_nominal.txt.gz

screen -S Fr_I_nTreg
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Fr_I_nTreg_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Fr_I_nTreg.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.all.txt
done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Fr_I_nTreg.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Fr_I_nTreg 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Fr_I_nTreg --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Fr_I_nTreg

### -- ENDOF Fr_I_nTreg -- ##




### -- Fr_II_eTreg -- ##
# Fraction II effector regulatory T cells: Fr_II_eTreg_nominal.txt.gz

screen -S Fr_II_eTreg
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Fr_II_eTreg_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Fr_II_eTreg.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Fr_II_eTreg.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Fr_II_eTreg 
done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Fr_II_eTreg --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/immunexut/Fr_II_eTreg

### -- ENDOF Fr_II_eTreg -- ##

	



### -- switched memory B; SM_B -- ##
# switched memory B: SM_B_nominal.txt.gz

screen -S SM_B
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/SM_B_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".SM_B.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.SM_B.tmp -o /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.SM_B.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.SM_B.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.SM_B & done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.SM_B --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/SM_B.MS

### -- ENDOF switched memory B -- ##




### -- Unswitched memory B; USM_B -- ##
# Unswitched memory B: USM_B_nominal.txt.gz

screen -S USM_B
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/USM_B_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".USM_B.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.USM_B.tmp -o /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.USM_B.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.USM_B.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.USM_B & done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.USM_B --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/USM_B.MS

### -- ENDOF switched memory B -- ##




### -- naive B -- ##
screen -S Naive_B
conda activate mesc
cd  /work/Tools/MESC/data/immunexut
## the resulting names are: Gene_name TSS_position Variant_ID CHR Variant_position_start sample_size nominal_P_value slope(ALT) 
zcat /archive/data/immunexut/nominal/Naive_B_nominal.txt.gz | awk 'NR>1 {print $0, "416" >$3".Naive_B.tmp"}'

## Rscript to estimate Z and format output to MESC required format
for i in {1..22}
do
 Rscript /work/Tools/MESC/data/immunexut/mesc_format.R -c hg38 -t Hg19 -i /work/Tools/MESC/data/immunexut/chr$i.Naive_B.tmp -o /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp -g  /work/Tools/MESC/data/immunexut/chr$i.Naive_B.all.txt & done


## remove SNPS IDs that are NAs and genes with the same name but different TSSs (it happens..)
#head -1 /work/Tools/MESC/data/immunexut/chr1.Th17.hg19.tmp > /work/Tools/MESC/data/immunexut/Th17.hg19.txt
for i in {1..22}
do
 awk 'NR>1 && $3!="NA"' /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp > /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp2
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp3
 cp /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp4
 cat /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp3 | awk 'x[$1]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp4 > /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp5
  cp /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp5 /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp4
 done  
 mv /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp4 /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt
 awk '!x[$1, $2]++ {print $1, $2}' /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt > /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp
 cp /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp2
 cat /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp | awk 'x[$2]++ {print $1}' | while read name
 do
  grep -v $name /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp2 > /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp3
  cp /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp3 /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp2
 done  
 cp /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.tmp2 /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt
 rm /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.tmp? /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt.*

done 


## estimate gene expression scores
for i in {1..22}
do
 python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.Naive_B.hg19.txt --out /work/Tools/MESC/mesc/data/immunexut/immunexut.Naive_B & done


### h2med
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.Naive_B --out  /work/Projects/MS-chromatin_fine_mapping/results/MESC/Naive_B.MS


### -- ENDOF naive B -- ##


### -- Unique gene sets -- ##
## there are two types: i) unique for each cell type within the CD4 T or B population, ii) unique vs. both CD4 T and B cells, other than the one of interest

## create gene sets
for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 SM_B USM_B Naive_B DN_B 
do
 echo -n $j"_unique_within_main_type " > /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets
 awk 'NR>1 && !x[$2]++ {print $2}' /work/Tools/MESC/data/immunexut/$i.un_eGenes | tr '\n' ' ' >> /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets
 echo "" >> /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets
 echo -n $j"_unique_across_CD4_B " >> /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets
 awk 'NR>1 && !x[$2]++ {print $2}' /work/Tools/MESC/data/immunexut/$j.un_non*_eGenes | tr '\n' ' ' >> /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets
 echo "" >> /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets

done

## create expression scores

for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 SM_B USM_B Naive_B DN_B 
#for j in Th17 SM_B USM_B Naive_B
do
 for i in {1..22}
 do
  python /work/Tools/MESC/mesc/run_mesc.py --compute-expscore-sumstat --eqtl-sumstat /work/Tools/MESC/data/immunexut/chr$i.$j.hg19.txt --gene-sets /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.genesets --out /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.unique 
 done
done


## MS (Science 2019)
mkdir -p /work/Projects/MS-chromatin_fine_mapping/results/MESC
for j in Th17 Fr_II_eTreg Fr_I_nTreg Th2 Th1 Naive_CD4 Fr_III_T Tfh Mem_CD4 SM_B USM_B Naive_B DN_B 
do
 echo $j
 mkdir -p /work/Projects/MS-chromatin_fine_mapping/results/MESC/$j
 python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/mesc/data/immunexut/immunexut.$j.unique --out /work/Projects/MS-chromatin_fine_mapping/results/MESC/$j/MS_2019
done

### -- ENDOF Unique gene sets -- ##



cd /work/Tools/MESC
git clone https://github.com/douglasyao/mesc.git
cd mesc
# install libraries
conda install -c anaconda numpy
conda install -c anaconda pandas
conda install -c anaconda bitarray
conda install -c anaconda scipy


### MESC test with provided data
## MS (Science 2019)
python /work/Tools/MESC/mesc/run_mesc.py --h2med /work/Tools/MESC/data/summaryStats/MS.final.sumstats.gz --exp-chr /work/Tools/MESC/data/eqtlgen_expscores/eqtlgen --out /work/Tools/MESC/test_eqtlgen



