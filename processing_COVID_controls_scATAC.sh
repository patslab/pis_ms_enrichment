### Healthy controls from https://www.ncbi.nlm.nih.gov/bioproject/PRJNA718009 
## Updated 10/12/2021.   
## location is in Socrates: /archive/data/public/Covid_controls/



### Cellranger was run already. See README in Socrates




cd /archive/data/public/Covid_controls

screen -S Covid_controls
conda activate r_env

## single files
cat SRR.list | while read file;
do
 echo $file
 ## Signac QC
 run=$file
 peaks=/archive/data/public/Covid_controls/$run/$run/outs/filtered_peak_bc_matrix.h5
 metadata=/archive/data/public/Covid_controls/$run/$run/outs/singlecell.csv
 fragments=/archive/data/public/Covid_controls/$run/$run/outs/fragments.tsv.gz
 outdir=/archive/data/public/Covid_controls/$run/QCed/hg38
 out=signac
 mkdir -p $outdir
 Rscript /work/Projects/Brainomics/scripts/single_sample_QC_scATAC_hg38.R -p $peaks -m $metadata -f $fragments -d $outdir -o $out >& $outdir/single_sample_QC_scATAC.log & done
 


=/work/Tools/cellranger-atac/refdata-cellranger-arc-GRCh38-2020-A-2.0.0 \
                   --fastqs=../ \
                   --sample=$file  \
                   --localcores=20 \
                   --localmem=150
## Signac QC
file=SRR11442501
screen -S $file
conda activate r_env
run=SRR11442501
peaks=/archive/data/public/Corces_2020_Nat_Gen/$run/$run/outs/filtered_peak_bc_matrix.h5
metadata=/archive/data/public/Corces_2020_Nat_Gen/$run/$run/outs/singlecell.csv
fragments=/archive/data/public/Corces_2020_Nat_Gen/$run/$run/outs/fragments.tsv.gz
outdir=/archive/data/public/Corces_2020_Nat_Gen/$run/QCed/hg38
out=signac
mkdir -p $outdir
Rscript /work/Projects/Brainomics/scripts/single_sample_QC_scATAC_hg38.R -p $peaks -m $metadata -f $fragments -d $outdir -o $out >& $outdir/single_sample_QC_scATAC.log 

#### missing files:
SAMN19128574/SRR14514155
SRR14514133
